$(document).ready(function() {
	// Start Clock
	function startTime(){
		var b=new Date();
		var d=b.getUTCHours();
		var a=b.getUTCMinutes();
		var c=b.getUTCSeconds();
		d=checkTime(d);
		a=checkTime(a);
		c=checkTime(c);
		document.getElementById("clock").innerHTML=d+":"+a+":"+c+" UTC";
		t=setTimeout(function(){startTime()},500)
	}
	function checkTime(a){if(a<10){a="0"+a}return a};
	startTime();

	// Load supplemental libraries once page is loaded
	var scripts = [
		'',
	];

	var styles = [
		'',
	];

	$.each(scripts,function(key,url){
		$.getScript(url);
	});

	$.each(styles,function(key,url){
		$("<link/>", {
		   rel: "stylesheet",
		   type: "text/css",
		   href: url,
		}).appendTo("head");
	});
});