var crewcenter = angular.module('crewcenter', ['ngRoute']);

crewcenter.config(function($routeProvider){
	console.log($routeProvider);
	$routeProvider

	.when('/',{
		templateUrl : '../../../pages/dashboard.html',
		controller : 'mainController'
	})

	.when('/about',{
		templateUrl : '../../../pages/dashboard.html',
		controller : 'aboutController'
	})

	.otherwise({
		redirectTo: '/dashboard'
	});
});

crewcenter.controller('mainController', function($scope) {
	console.log('main');
	$scope.message = 'Everyone come and see how good I look!';
});
crewcenter.controller('aboutController', function($scope) {
	console.log('about');
	$scope.message = 'about page';
});